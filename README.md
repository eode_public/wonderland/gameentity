**This is the home page of [this documentation](http://wonderland.eode.studio/docs/gameentity-_introduction.html)**

## Introduction

### Version
> Released, actively maintained

### Dependencies
- `Common`

## Introduction
GameEntity is a framework for systemic entity building that allows for emergent gameplay. A **GameEntity**'s interactions are defined by **GameEntityAspects** (e.g. Damageable, Slowable, Destructible) and its caracteristics with **Stats** that you can affect at runtime by adding and removing **StatModifiers** (Slow, Damage bonus, etc...).

## Example
![GameEntity](http://wonderland.eode.studio/GameEntity/inspector.png)
![GameEntity](http://wonderland.eode.studio/GameEntity/inspector2.png)